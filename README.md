# README #

Assignments for Front-End JavaScript Frameworks: Introduction to AngularJS taught by Prof. Jogesh K. Muppala, Hong Kong University on Coursera: Jan 11th - Feb 8th, 2016.


### Topics covered in Week 1 ###
* Quick introduction to front-end JavaScript frameworks, 
* Angular built-in directives, 
* Model-View-Controller framework, 
* Angular Modules
* Angular Controllers
* Angular Filters

### Tools and Softwares ###

* AngularJS
* Bootstrap
* Bower

### Who do I talk to? ###

* Vignesh Subramanian


All works presented are original.